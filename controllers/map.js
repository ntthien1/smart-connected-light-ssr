"use strict";
/*
 * GET map
 */

var airvantage = require('../model/airvantage');
var _ = require('underscore');
var async = require('async');

exports.get = function (req, resp) {

    airvantage.device_list({ access_token: req.session.token })(function (err, res) {
        if (err) {
            console.log("ERR: " + err);
            // next(err);
        } else {

            async.parallel(
                _.map(res, function (item) {

                    return airvantage.user_device_info({ uid: item.device_id, access_token: req.session.token })
                }), function (err, res) {
                    if (err) {
                        console.log("ERR: " + err);
                        return [];
                    } else {
                        resp.render('map', {
                            maps: res,
                            access_token: req.session.token,
                            userMail: req.session.email,
                            handleViewLine: "setViewLine(value);"
                        });
                    }

                });
        }
    });
}
